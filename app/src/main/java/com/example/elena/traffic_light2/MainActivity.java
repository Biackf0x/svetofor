package com.example.elena.traffic_light2;

import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import static android.icu.lang.UCharacter.GraphemeClusterBreak.L;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);}

    public void onClickBtnRed(View view) {
        getWindow().setBackgroundDrawableResource(R.color.colorRed);
    }
    public void onClickBtnYellow(View view) {
        getWindow().setBackgroundDrawableResource(R.color.colorYellow);

    }
    public void onClickBtnGreen(View view) {
        getWindow().setBackgroundDrawableResource(R.color.colorGreen);
    }
}


